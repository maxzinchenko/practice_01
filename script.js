const QUESTIONS_PER_TICKET = 20, MAX_MISTAKES_ALLOWED = 2;

const keys = object => Object.keys(object).map(Number);

const gerRandomIndexFromArray = array => Math.round(Math.random() * (array.length - 1));

const cutRandomElement = array => array.splice(gerRandomIndexFromArray(array), 1)[0];

const questionIds = keys(questions);

const applicantIds = keys(applicants);

const tickets = {};

const mistakesCount = exam => exam.answers
    .filter((answer, index) => questions[tickets[exam.ticketId][index]].check !== answer)
    .length;

const isPassed = exam => exam.answers.length === 20 && mistakesCount(exam) <= MAX_MISTAKES_ALLOWED;

const isFailed = exam => mistakesCount(exam) > MAX_MISTAKES_ALLOWED;

for (let i = 0; i < applicantIds.length; i++) {
    tickets[i + 1] = [];
    for (let j = 0; j < QUESTIONS_PER_TICKET; j++) {
        let n;
        do {
            n = gerRandomIndexFromArray(questionIds);
        } while (tickets[i + 1].includes(questionIds[n]));

        tickets[i + 1].push(questionIds[n]);
    }
}

const ticketIds = keys(tickets);

const exams = [];
for (const applicantId of applicantIds) {
    const ticketId = cutRandomElement(ticketIds);

    const answers = [];

        for (let qNumber = 0; qNumber < QUESTIONS_PER_TICKET && !isFailed({ answers, ticketId }); qNumber++) {
            const question = questions[tickets[ticketId][qNumber]];

        if (Math.random() > (MAX_MISTAKES_ALLOWED + 0.625) / QUESTIONS_PER_TICKET) {
            answers.push(question.check);
        } else {
            answers.push(cutRandomElement(keys(question.answers).filter(i => i !== question.check)));
        }
    }

    exams.push({
       applicantId,
       ticketId,
        answers
    });
}
// console.log(exams);


const passed = exams.filter(isPassed);
console.group(`Passed: ${ passed.length }`);
console.table(passed.map(exam => ({
    Applicant: applicants[exam.applicantId],
    'Correct Answers': QUESTIONS_PER_TICKET - mistakesCount(exam),
})));
console.groupEnd();


// const failed = exams.filter(isFailed);
// console.groupCollapsed(`Failed : ${failed.length}`);
// console.log(failed.map(exam => ({
//     Applicant: applicants[exam.applicantId],
//     Skipped: QUESTIONS_PER_TICKET - exam.answers.length,
//     'Correct Answers': exam.answers.length - MAX_MISTAKES_ALLOWED -1
// })));
// console.groupEnd()
// console.log(passed, failed);

const failed = exams.filter(isFailed);
console.group(`Failed: ${ failed.length }`);
console.table(failed.map(exam => ({
    Applicant: applicants[exam.applicantId],
    Skipped: QUESTIONS_PER_TICKET - exam.answers.length,
    'Correct Answers': exam.answers.length - MAX_MISTAKES_ALLOWED - 1
})));
console.groupEnd();